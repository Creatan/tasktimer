from flask import Flask, render_template, jsonify, request
from mongoengine import connect
from flask.ext.mongoengine import MongoEngine


app = Flask(__name__)
app.config.from_object('config')

db = MongoEngine(app)

@app.errorhandler(404)
def not_found(error):
	return render_template('404.html'), 404

def register_blueprints(app):
	from taskTimer.views import entries
	app.register_blueprint(entries)

register_blueprints(app)