from taskTimer import db
import datetime

class Entry(db.Document):
	created_at = db.DateTimeField(default=datetime.datetime.now, 
		required=True)
	description = db.StringField(max_length=255, required=True)
	time = db.IntField(required=True)
	billed = db.BooleanField()

	meta = {
		'ordering':['created_at']
	}