from functools import wraps
from flask import Blueprint, render_template, jsonify, request, session, redirect, url_for
from taskTimer.model import Entry
from taskTimer import app

entries = Blueprint('entries',__name__,template_folder="templates")


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get('logged_in') is None:
            return redirect(url_for('.login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

def auth(user, password):
    print user
    if user == app.config['USER'] and password == app.config['PASS']:
        return True
    else:
        return False


@entries.route('/')
@login_required
def index():
    return render_template('index.html');

@entries.route('/save',methods=['POST'])
@login_required
def save():
    
    entry = Entry(
        time = request.form['time'],
        description = request.form['description'])

    entry.save()
    #return inserted object
    return jsonify(result=entry.to_json())

@entries.route('/list',methods=['GET'])
@login_required
def list():
    entries = Entry.objects.all()

    return jsonify(result=entries.to_json())


@entries.route('/update/<id>',methods=['POST'])
@login_required
def update(id):
    entry = Entry.objects.get(pk=id)
    entry.update(**{'set__billed':True})

    return jsonify(result=entry.to_json())
@entries.route('/logout')
@login_required
def logout():
   session.pop('logged_in',None)
   return redirect(url_for('.login'))

@entries.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        print request.form['username']
        user = auth(request.form['username'],request.form['password'])
        if user:
            session['logged_in'] = True
            return redirect(url_for('.index'))
        else:
            return render_template('login.html')
    return render_template('login.html')