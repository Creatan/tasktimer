/*
 * jQuery Tiny Pub/Sub
 * https://github.com/cowboy/jquery-tiny-pubsub
 *
 * Copyright (c) 2013 "Cowboy" Ben Alman
 * Licensed under the MIT license.
 */

(function($) {

    var o = $({});

    $.subscribe = function() {
        o.on.apply(o, arguments);
    };

    $.unsubscribe = function() {
        o.off.apply(o, arguments);
    };

    $.publish = function() {
        o.trigger.apply(o, arguments);
    };

}(jQuery));

window.helpers = {};

window.helpers.pad = function pad(val) {
    return val > 9 ? val : "0" + val;
}
//get rid of global entries
window.helpers.entries = []

var Clock = function() {
    this.tick = 0;
}
Clock.prototype.pad = helpers.pad;

Clock.prototype.start = function() {
    self = this;
    this.timer = setInterval(function() {
        ++self.tick;
        $('.timer').html(self.pad(parseInt(self.tick / (60 * 60), 10)) + ':' +
            self.pad(parseInt(self.tick % (60 * 60) / 60, 10)) + ':' + self.pad(self.tick % 60));
    }, 1000);
};
Clock.prototype.stop = function() {
    if (typeof this.timer !== 'undefined') {
        clearInterval(this.timer);
    }
};
Clock.prototype.reset = function() {
    this.tick = 0;
    this.stop();
    $('.timer').html('00:00:00');
}

function drawTable() {
    $('table.entries tbody').html('');
    $.each(helpers.entries, function(key, val) {
        var row = $('<tr></tr>');
        row.append($('<td/>').html(val.description));
        row.append($('<td/>').html(helpers.pad(parseInt(val.time / (60 * 60), 10)) + ':' +
            helpers.pad(parseInt(val.time % (60 * 60) / 60, 10)) + ':' + helpers.pad(val.time % 60)));
        row.append($('<td/>').html(helpers.pad(val.inserted.getDate()) + '.' + helpers.pad(val.inserted.getMonth() + 1) + '.' +
            val.inserted.getFullYear()));
        row.append($('<td/>').html(billedHtml(val)));
        $('table.entries tbody').append(row);
    });

}

function billedHtml(val) {
    if (val.billed) {
        return '<span class="glyphicon glyphicon-ok billed"></span>';
    } else {
        return '<a class="update" href="/update/' + val.id + '"><span class="glyphicon glyphicon-plus"></span></a>';
    }
}

function addEntries(data) {
    var result = $.parseJSON(data.result);
    if ($.isArray(result)) {
        $.each(result, function(index, val) {
            helpers.entries.push({
                id: val._id.$oid,
                time: val.time,
                description: val.description,
                inserted: new Date(val.created_at.$date),
                billed: val.billed
            });
        });
    } else {
        helpers.entries.push({
            id: result._id.$oid,
            time: result.time,
            description: result.description,
            inserted: new Date(result.created_at.$date),
            billed: result.billed
        });
    }

    $.publish('refreshTable')
}

$(document).ready(function() {

    //var entries = [];
    $.subscribe('refreshTable', drawTable);

    $.ajax({
        url: '/list',
        type: 'GET',
        dataType: 'json',
        success: addEntries,
    })

    var clock = new Clock();
    //form input for storing and submittin lapsed time
    var time = document.forms[0].time;

    $('.container').on('click', 'button', function() {
        var $this = $(this);
        if ($this.hasClass('start')) {
            clock.start();
            $this.removeClass('start').addClass('stop');
            $this.find('span').removeClass('glyphicon-play').addClass('glyphicon-pause');

        } else {
            clock.stop();
            time.value = clock.tick;
            $this.removeClass('stop').addClass('start');
            $this.find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');
        }

    });
    $('form.time').on("submit", function(e) {
        e.preventDefault();
        time.value = clock.tick;
        clock.reset();

        $('.stop').removeClass('stop').addClass('start')
            .find('span').removeClass('glyphicon-pause').addClass('glyphicon-play');

        $.ajax({
            url: '/save',
            type: 'POST',
            data: $('form').serialize(),
            success: addEntries,
            error: function(error) {
                console.log(error)
            }
        });
    });
    $('table').on('click', '.update', function(e) {

        e.preventDefault();
        $this = $(this);
        $.ajax({
            url: this.href,
            type: 'POST',
            success: function(data) {
                $this.parents('td').html(billedHtml({
                    billed: true
                }));
            },
            error: function(error) {
                console.log(error);
            }
        });

    });

});